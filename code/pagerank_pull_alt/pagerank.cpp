// Parallel Pagerank implementation
// CSR graph representation

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include <math.h>
#include <omp.h>

#include "../Timer.hpp"
#include "graph.hpp"

static inline unsigned int get_out_degree(graph_t *G, int vertex_idx) {
	return G->first_neighbor_indices[vertex_idx + 1] -  G->first_neighbor_indices[vertex_idx];
}

void pagerank (graph_t *G, int max_iter, float scale_factor) {
  int num_vertices = G->n_vertices;
  //float *rank = new float[num_vertices];
  //float *new_rank = new float[num_vertices];
  float global_add = (1.0 - scale_factor) / (float)num_vertices;

  /* Set initial value */
  float initial = 1.0 / (float)num_vertices;

  vertex_t* vertices = G->vertices;

  #pragma omp parallel for
  for (int i = 0; i < num_vertices; i++) {
   	 unsigned int out_degree = get_out_degree(G, i);
	 if (out_degree > 1) {
 	    vertices[i].pagerank = initial / (float)out_degree;
	 } else {
	 	vertices[i].pagerank = initial;
	 }
  }

  int iteration = 0;
  while (iteration++ < max_iter) { 
    float dangling_sum = 0.0f;
    #pragma omp parallel for reduction(+:dangling_sum)
    for(int i = 0; i < num_vertices; i++) {
      if (get_out_degree(G, i) == 0) {
          dangling_sum += vertices[i].pagerank;
      }
    }
    float disconnected_compensation = dangling_sum / (float)num_vertices;

    #pragma omp parallel for
    for(int i = 0; i < num_vertices; i++) {
	  float newrank = disconnected_compensation;
      unsigned int start = G->first_parent_indices[i];
      unsigned int end = G->first_parent_indices[i + 1];


      for (int e = start; e < end; e++) {
	    unsigned int neighbor_index = G->parents[e];
	    newrank += vertices[neighbor_index].pagerank;
	  }
	  vertices[i].newpagerank = newrank;
    }

    // Consolidate rank
    #pragma omp parallel for
    for (int i = 0; i < num_vertices; i++) {
    	// Apply dampening
    	vertices[i].pagerank = global_add + (scale_factor * vertices[i].newpagerank);
    	if (iteration < max_iter) {
    		 unsigned int out_degree = get_out_degree(G, i);
			 if (out_degree > 1) {
			 	vertices[i].pagerank = vertices[i].pagerank / out_degree;
			 }
    	}
    }
  }
  
  /* Print results */
  /*
  float sum = 0;
  for(int i = 0; i < num_vertices; i++) {
    std::cout << "(" << i << ", " << vertices[i].pagerank << ")\n";
    sum += vertices[i].pagerank;
  }
  std::cout << "PageRank sum: " << sum << "\n";
  */
 }

int main (int argc, char* argv[]) {
  if (argc != 2) {
    std::cout << "usage:  pagerank <filename>\n";
    return 0;
  }

  std::cout << "Reading from file " << argv[1] << " ...\n";
  graph_t* g = csr_graph_from_file (argv[1]);
  std::cout << "Graph contains " << g->n_edges << " edges, " << g->n_vertices << " vertices\n";
  std::cout << "--------------------------\n";

  int thr[] = {60, 240}; 
  for (int t = 0; t < 2; t++) {
    TimerRegister tr;
    Timer timer(tr, "Pagerank timer (ran 10x)");
    printf("Set number of threads to %d\n", thr[t]);
    omp_set_num_threads(thr[t]);
    for (int i = 0; i < 10; i++) {
      timer.start();
      pagerank(g, 10, 0.85f);
      timer.stop();
    }
    tr.print_results(std::cout);

  }

  free(g);

  return 0;
}

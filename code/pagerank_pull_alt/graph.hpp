#include <atomic>

/* Vertex struct  */
typedef struct vertexstruct {
    unsigned int level;
    std::atomic<bool> visited;
    float pagerank;
    float newpagerank;
} vertex_t;

/* CSR datastructure  */
typedef struct graphstruct { // A graph in compressed-adjacency-list (CSR) form
  vertex_t *vertices;
  unsigned int n_vertices;            // number of vertices
  unsigned int n_edges;            // number of edges
  unsigned int *neighbors;          // array of neighbors of all vertices
  unsigned int *parents;
  unsigned int *first_neighbor_indices;     // index in neighbors[] of first neighbor of each vtx
  unsigned int *first_parent_indices;
  unsigned int frontier_size;
} graph_t;

graph_t* csr_graph_from_file(char* filename);
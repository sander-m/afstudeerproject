// Collaborative filtering

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include <math.h>
#include <omp.h>
#include <numeric>
#include <functional>
#include <vector>
#include <map>
#include <iterator>

#include "../Timer.hpp"
#include "cf.hpp"
#include <mkl.h>

static const float LRATE = 0.001;
static const float K = 0.02;
static const float FEATURE_INIT_VALUE = 0.1;
static const float MIN_IMPROVEMENT = 0.0001;
static const int NUM_FEATURES = 64;
static const int MIN_ITERATIONS = 120;

static const int USE_MKL = 0;

static inline float cap_score(float score) {
  if (score > 5) {
    score = 5;
  }
  if (score < 1) {
    score = 1;
  }
  return score;
}

static inline float get_matrix_rating(unsigned int user, unsigned int movie, 
  context *c) {
  std::vector<float> &user_features = (*c->user_features)[user];
  std::vector<float> &movie_features = (*c->movie_features)[movie];

  // Computes the inner product of user and feature vector corresponding to a user and movie
  float score = 1.0f;
  if (USE_MKL == 0) {
    //#pragma simd
    for (unsigned int f = 0; f < NUM_FEATURES + 1; f++) {
      score += user_features[f] * movie_features[f];
    }
  } else {
      score += cblas_sdot(NUM_FEATURES, &user_features[0], 1, &movie_features[0], 1);
  }
  
  return cap_score(score);
}

static inline float predict_rating(const unsigned int cur_feature,
                                  std::vector<float> & user_feature_vector,
                                  std::vector<float> & movie_feature_vector) {
  // Computes the inner product of user and feature vector corresponding to a user and movie
  float score = 1.0;
  if (USE_MKL == 0) {
    //#pragma simd
    for (unsigned int f = 0; f < cur_feature + 1; f++) {
      score += user_feature_vector[f] * movie_feature_vector[f];
    }
    score += (NUM_FEATURES - cur_feature - 1) * (FEATURE_INIT_VALUE * FEATURE_INIT_VALUE);
  } else {
    score += cblas_sdot(cur_feature + 1, &user_feature_vector[0], 1, &movie_feature_vector[0], 1);
    score += (NUM_FEATURES - cur_feature - 1) * (FEATURE_INIT_VALUE * FEATURE_INIT_VALUE);
  }
  return cap_score(score);
}

context* create_context(std::vector<rating> &ratings, unsigned int max_user_id, unsigned int max_movie_id) {
  context* c = new context();
  c->user_features = new std::vector<std::vector<float>>();
  c->movie_features = new std::vector<std::vector<float>>();

  c->user_features->reserve(max_user_id + 1);
  c->movie_features->reserve(max_movie_id + 1);

  /* Initialize feature vector for every user and every movie */
  for(int i = 0; i < max_user_id + 1; i++) {
	  std::vector<float> user_feature_vector(NUM_FEATURES, FEATURE_INIT_VALUE);
	  c->user_features->push_back(user_feature_vector);
  }

  for(int i = 0; i < max_movie_id + 1; i++) {
	  std::vector<float> movie_feature_vector(NUM_FEATURES, FEATURE_INIT_VALUE);
	  c->movie_features->push_back(movie_feature_vector);
  }

  return c;
}

context* collaborative_filtering(std::vector<rating> &ratings, int max_user_id, int max_movie_id) {
  context *c = create_context(ratings, max_user_id, max_movie_id);
  unsigned int num_ratings = ratings.size();
  
  for (unsigned int f = 0; f < NUM_FEATURES; f++) {
    float rmse = 2;
    float rmse_prev = 0.0;

    for(unsigned int i = 0; (i < MIN_ITERATIONS || rmse < rmse_prev - MIN_IMPROVEMENT); i++) {
      rmse_prev = rmse;
      float squared_err = 0.0;

      for (unsigned int r = 0; r < num_ratings; r++) {
        rating ra = ratings[r];
        std::vector<float>& user_feature_vector = (*c->user_features)[ra.user];
    	  std::vector<float>& movie_feature_vector = (*c->movie_features)[ra.movie]; 

        float err = 1.0 * ra.rating - predict_rating(f, user_feature_vector, movie_feature_vector);
        float uv = user_feature_vector[f];
        user_feature_vector[f] += LRATE * (err * movie_feature_vector[f] - K * uv);
        movie_feature_vector[f] += LRATE * (err * uv - K * movie_feature_vector[f]);

        squared_err += err * err;
      }
      rmse = sqrt(squared_err / num_ratings);
      //printf("RMSE: %f\n", rmse);
    }

  }

  //print_matrix(c, max_user_id, max_movie_id);
  //print_results(c, max_user_id, max_user_id);
  return c;
}

void read_data(char* filename, std::vector<rating> &ratings, int* max_user, int* max_movie) {
  unsigned int user_id, item_id, t;
  unsigned int value;
  FILE *input = fopen(filename, "r");

  // Skips # comment lines
  int count = 0;
  char buf[0x1000];
  int max_movie_id = 0;
  int max_user_id = 0;
  while (fgets(buf, sizeof(buf), input) != NULL) {
      if (buf[0] != '#') {
        int read = sscanf(buf, "%i %i %i %i", &user_id, &item_id, &value, &t);
        if (read != 4) {
          break;
        }
        //matrix[user_id][item_id] = rating;
        rating* r = (rating *) calloc(1, sizeof(rating));
        r->user = user_id;
        r->movie = item_id;
        r->rating = value;
        ratings.push_back(*r);
        count++;

        if (user_id > max_user_id) {
          max_user_id = user_id;
        }
        if (item_id > max_movie_id) {
          max_movie_id = item_id;
        }
      }
  }
  fclose(input);  

  printf("Read %d ratings\n", count);
  printf("Max user ID: %d\n", max_user_id);
  printf("Max movie ID: %d\n", max_movie_id);
  *max_movie = max_movie_id;
  *max_user = max_user_id;
}

void free_all(context *c) {
  delete c->user_features;
  delete c->movie_features;
  delete c;
}

void print_matrix(context *c, unsigned int max_user_id, unsigned int max_movie_id) {
   // Print prediction matrix
  printf("----- PREDICTION MATRIX ------\n");
  printf("        ");
  for (int j = 0; j < max_movie_id + 1; j++) {
    printf("MOVIE %d    ", j);
  }
  printf("\n");
  for (int i = 0; i < max_user_id + 1; i++) {
    printf("USER %d: ", i);
    for (int j = 0; j < max_movie_id + 1; j++) {

      float pred = get_matrix_rating(i, j, c);
      printf("%f   ", pred);
    }
    printf("\n");
  }
  printf("------------------------------\n");
}

void print_results(context *c, unsigned int max_user_id, unsigned int max_movie_id) {
     // Print prediction matrix
  printf("----- PREDICTIONS ------\n");
  float sum = 0;
  int count = 0;
  for (int i = 0; i < max_user_id + 1; i++) {
    for (int j = 0; j < max_movie_id + 1; j++) {
      float pred = get_matrix_rating(i, j, c);
      sum += pred;
      count++;
    }
  }
  printf("Average rating: %f\n", (float) (sum / count));
  printf("------------------------------\n");
}


int main (int argc, char* argv[]) {
  if (argc != 2) {
    std::cout << "usage: cf <filename>\n";
    return 0;
  }
  std::cout << "Reading from file " << argv[1] << " ...\n";
  
  std::vector<rating> ratings;
  ratings.reserve(10000);

  int max_user_id, max_movie_id;
  read_data(argv[1], ratings, &max_user_id, &max_movie_id);
  //printf("Max user ID %d, max movie ID %d", max_user_id, max_movie_id);

  TimerRegister tr;
  Timer timer(tr, "CF timer (ran 10x)");
  for (int i = 0 ; i < 10; i++) {
    timer.start();
    context *c = collaborative_filtering(ratings, max_user_id, max_movie_id);
    timer.stop();
    free_all(c);
  }
  tr.print_results(std::cout);
    
  //tr.print_results(std::cout);

  return 0;
}

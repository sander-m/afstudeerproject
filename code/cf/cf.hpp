#include <stdlib.h>
#include <vector>

typedef struct {
  unsigned int user;
  unsigned int movie;
  unsigned int rating;
} rating;

typedef struct context {
  std::vector<std::vector<float>> *user_features;
  std::vector<std::vector<float>> *movie_features;
} context;

float get_matrix_rating(unsigned int user_feature, unsigned int movie, context *c);
static inline float cap_score(float score);
static inline float predict_rating(const unsigned int cur_feature,
                                  std::vector<float> & user_feature_vector,
                                  std::vector<float> & movie_feature_vector);
context* create_context(std::vector<rating> &ratings, unsigned int max_user_id, unsigned int max_movie_id);
context* collaborative_filtering(std::vector<rating> &ratings, int max_user_id, int max_movie_id);
void read_data(char* filename, std::vector<rating> &ratings, int* max_user, int* max_movie);
void free_all(context *c);
void print_matrix(context *c, unsigned int max_user_id, unsigned int max_movie_id);
void print_results(context *c, unsigned int max_user_id, unsigned int max_movie_id);
// Breadth first search implementation
// Uses CSR representation

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include <math.h>
#include <omp.h>

#include "../Timer.hpp"
#include "graph.hpp"
#include "tbb/concurrent_queue.h"

void bfs (int start_vertex_idx, graph_t *G, std::vector<unsigned int> *level_counts) {
  tbb::concurrent_bounded_queue<unsigned int> *current_queue = new tbb::concurrent_bounded_queue<unsigned int>;
  tbb::concurrent_bounded_queue<unsigned int> *next_queue = new tbb::concurrent_bounded_queue<unsigned int>;

  vertex_t *start_vertex = &G->vertices[start_vertex_idx];
  start_vertex->level = 1;
  start_vertex->visited = true;
  current_queue->push(start_vertex_idx);

  unsigned int cur_level = 1;
  unsigned int next_queue_size = 0;

  if (level_counts != NULL) {
    level_counts->clear();  
  }

  while (!current_queue->empty()) {
      next_queue->clear();

      /* Process queue in parallel (level-synchronous) */
      int cur_size = current_queue->size();
      #pragma omp parallel shared(current_queue, next_queue, G)
      for (unsigned int i = 0; i < cur_size; i++) {
            unsigned int u = 0;
            if(!current_queue->try_pop(u)) {
            	continue;
            }

            unsigned int start = G->first_neighbor_indices[u];
            unsigned int end = G->first_neighbor_indices[u + 1];

            for (unsigned int e = start; e < end; e++) {
              // w is the current neighbor of v
              unsigned int vertex_index = G->neighbors[e];
              vertex_t *w = &G->vertices[vertex_index];
              if (!w->visited.exchange(true)) {
                // Visit w
                w->level = cur_level;
                // put w on queue to explore
                next_queue->push(vertex_index);
              }
            }
       }

      // Threads synchronized at this point

      if (level_counts != NULL) {
        level_counts->push_back(cur_size);
      }
      cur_level++;

      // Swap current and next queue
      std::swap(next_queue, current_queue);
  }

  delete current_queue;
  delete next_queue;
}

void benchmark_bfs(graph_t *g) {
  TimerRegister tr;
  Timer timer(tr, "BFS timer (BFS executed for all nodes)");
  std::vector<unsigned int> level_counts;
  // Go through all vertices

  timer.start();
  for (int start = 0; start < 1; start++) {
    for (int v = 0; v < g->n_vertices; v++) {
      g->vertices[v].visited = false;
    }
    bfs (start, g, NULL);
  }

  timer.stop();
  tr.print_results(std::cout);
}

int main (int argc, char* argv[]) {
  unsigned int i, start_vertex;
  if (argc == 3) {
    start_vertex = atoi (argv[2]);
  } else {
    std::cout << "usage:    bfs <filename> <start_vertex>\n";
    return 0;
  }

  std::cout << "Reading from file " << argv[1] << " ...\n";
  graph_t *g = csr_graph_from_file (argv[1]);
  std::cout << "--------------------------\n";
  
  printf("Starting benchmark\n");
  
  for (int threads = 1; threads <= 64; threads *= 2) {
    printf("Num threads set to %d\n", threads);
    omp_set_num_threads(threads);
    benchmark_bfs(g);
  }

  free(g->vertices);


  return 0;
}
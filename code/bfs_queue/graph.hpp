#include <atomic>

class Vertex {
    public:
        std::atomic<bool> visited;
        int level;
};

/* Vertex struct  */
typedef struct vertexstruct {
    unsigned int level;
    std::atomic<bool> visited;
} vertex_t;

/* CSR datastructure  */
typedef struct graphstruct { // A graph in compressed-adjacency-list (CSR) form
  unsigned int n_vertices;            // number of vertices
  unsigned int n_edges;            // number of edges
  unsigned int *neighbors;          // array of neighbors of all vertices
  vertex_t *vertices;
  unsigned int *first_neighbor_indices;     // index in neighbors[] of first neighbor of each vtx

} graph_t;

graph_t* csr_graph_from_file(char* filename);
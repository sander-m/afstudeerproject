// Sequential implementation of breadth-first search
// Inspired by http://www.cs.ucsb.edu/~gilbert/cs140/old/cs140Win2011/bfsproject/bfs.html

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <omp.h>

#include "../Timer.hpp"
#include "concurrentqueue.h"
#include "graph.hpp"

void print_CSR_graph (graph_t *G) {
  int vlimit = 20;
  int elimit = 50;
  int e,v;
  printf("\nGraph has %d vertices and %d edges.\n",G->n_vertices,G->n_edges);
  printf("firstnbr =");
  if (G->n_vertices < vlimit) vlimit = G->n_vertices;
  for (v = 0; v <= vlimit; v++) printf(" %d",G->first_neighbor_indices[v]);
  if (G->n_vertices > vlimit) printf(" ...");
  printf("\n");
  printf("nbr =");
  if (G->n_edges < elimit) elimit = G->n_edges;
  for (e = 0; e < elimit; e++) printf(" %d",G->neighbors[e]);
  if (G->n_edges > elimit) printf(" ...");
  printf("\n\n");
}


void bfs (int s, graph_t *G, int **levelp, int *nlevelsp, 
         int **levelsizep, int **parentp) {

  int *level, *levelsize, *parent;
  int thislevel;
  int i, v, w, e;
  level = *levelp = (int *) calloc(G->n_vertices, sizeof(int));
  levelsize = *levelsizep = (int *) calloc(G->n_vertices, sizeof(int));
  parent = *parentp = (int *) calloc(G->n_vertices, sizeof(int));

  moodycamel::ConcurrentQueue<int> queue;
  for (v = 0; v < G->n_vertices; v++) level[v] = -1;
  for (v = 0; v < G->n_vertices; v++) parent[v] = -1;

  // assign the starting vertex level 0 and put it on the queue to explore
  thislevel = 0;
  level[s] = 0;
  levelsize[0] = 1;
  queue.enqueue(s);

  // loop over levels, then over vertices at this level, then over neighbors
  while (levelsize[thislevel] > 0) {
    levelsize[thislevel+1] = 0;

    for (i = 0; i < levelsize[thislevel]; i++) {
      queue.try_dequeue(v);
      
      // v is the current vertex to explore from
      int start = G->first_neighbor_indices[v];
      int end = G->first_neighbor_indices[v + 1];
      for (e = start; e < end; e++) {
        // w is the current neighbor of v
        w = G->neighbors[e]; 
        if (level[w] == -1) {   
          parent[w] = v;
          level[w] = thislevel + 1;
          levelsize[thislevel + 1]++;

          queue.enqueue(w);
        }
      }
    }
    thislevel++;
  }

  *nlevelsp = thislevel;
}

int main (int argc, char* argv[]) {
  graph_t *G;
  int *level, *levelsize, *parent;
  int nlevels;
  int startvtx;
  int i, v, reached;

  if (argc == 3) {
    startvtx = atoi (argv[2]);
  } else {
    printf("usage:   bfstest <startvtx> < <edgelistfile>\n");
    printf("example: cat sample.txt | ./bfstest 1\n");
    exit(1);
  }
  G = csr_graph_from_file (argv[1]);
  print_CSR_graph (G);

  printf("Starting vertex for BFS is %d.\n\n",startvtx);
  TimerRegister tr;
  Timer timer(tr, "mytimer");
  timer.start();
 
  bfs (startvtx, G, &level, &nlevels, &levelsize, &parent);
  
  timer.stop();
  tr.print_results(std::cout);

  reached = 0;
  for (i = 0; i < nlevels; i++) reached += levelsize[i];
  printf("Breadth-first search from vertex %d reached %d levels and %d vertices.\n",
    startvtx, nlevels, reached);
  for (i = 0; i < nlevels; i++) printf("level %d vertices: %d\n", i, levelsize[i]);
  if (G->n_vertices < 20) {
    printf("\n  vertex parent  level\n");
    for (v = 0; v < G->n_vertices; v++) printf("%6d%7d%7d\n", v, parent[v], level[v]);
  }
  printf("\n");
  
}





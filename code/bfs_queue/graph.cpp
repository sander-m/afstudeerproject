#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <vector>
#include "graph.hpp"

std::vector<std::pair<int, int>> * read_edges(char* filename, unsigned int* num_vertices) {
  unsigned int read_ints, from, to;
  FILE *input = fopen(filename, "r");
  std::vector<std::pair<int, int>> *edges = new std::vector<std::pair<int, int>>();

  // Skips # comment lines
  long max_edge = 0;
  char buf[0x1000];
  while (fgets(buf, sizeof(buf), input) != NULL) {
      if (buf[0] != '#') {
        read_ints = sscanf(buf, "%i %i", &from, &to);
        if (read_ints != 2) {
          break;
        }
        edges->push_back(std::pair<int, int>(from, to));

        if (from > max_edge) {
          max_edge = from;
        }
        if (to > max_edge) {
          max_edge = to;
        }
      }
  }
  fclose(input);  

  *num_vertices = max_edge + 1;
  return edges;
}

graph_t * csr_graph_from_file(char* filename) {
  unsigned int num_vertices;
  std::vector<std::pair<int, int>>* edges = read_edges(filename, &num_vertices);

  unsigned int i, e, v;
  graph_t *G = (graph_t *) calloc(1, sizeof(graph_t));
  G->n_edges = edges->size();

  G->n_vertices = num_vertices;
  G->neighbors = (unsigned int*) calloc(G->n_edges, sizeof(unsigned int));
  G->first_neighbor_indices = (unsigned int *) calloc(G->n_vertices + 1, sizeof(unsigned int));

  // count neighbors of vertex v in firstnbr[v+1],
  for (e = 0; e < G->n_edges; e++) {
    G->first_neighbor_indices[(*edges)[e].first + 1]++;
  }

  // cumulative sum of neighbors gives firstnbr[] values
  //printf("Number of vertices: %d\n", G->n_vertices);
  for (v = 0; v < G->n_vertices; v++) {
    G->first_neighbor_indices[v + 1] += G->first_neighbor_indices[v];
  }

  G->vertices = (vertex_t* ) calloc(G->n_vertices, sizeof(vertex_t));

  // pass through edges, slotting each one into the CSR structure
  for (e = 0; e < G->n_edges; e++) {
    i = G->first_neighbor_indices[(*edges)[e].first]++;
    //printf("i: %d",)
    G->neighbors[i] = (*edges)[e].second; //(vertex *) malloc(sizeof(vertex));
    //printf("(i = %d, id = %d)\n", i, G->neighbors[i]->id);
  }

  // the loop above shifted firstnbr[] left; shift it back right
  for (v = G->n_vertices; v > 0; v--) {
    G->first_neighbor_indices[v] = G->first_neighbor_indices[v-1];
  }
  G->first_neighbor_indices[0] = 0;

  return G;
}
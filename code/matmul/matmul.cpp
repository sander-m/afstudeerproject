// Matrix multiplication

#include <stdio.h>

#include <math.h>
#include <omp.h>
#include <mkl.h>
#include <algorithm>
#include "../Timer.hpp"

static const int MATRIX_SIZE = 10000;
static const int REPEAT = 2;

static inline float* matrix_multiply(float* matrixA, int rowsA, int colsA, 
                       float* matrixB, int rowsB, int colsB,
                       float* result_matrix) {

	/* Optimized intel MKL routine for matrix multiplication s*/
	cblas_sgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans,
	    rowsA,
	    colsB,
	    rowsB, 
	    1.0f, 
	    matrixA, 
	    colsA, 
	    matrixB, 
	    colsB, 
	    0.0f, 
	    result_matrix, 
	    colsB);
	return result_matrix;
}

int main (int argc, char* argv[]) {
  printf("Allocating memory...\n");
  float* matrixA = (float*)mkl_calloc(MATRIX_SIZE * MATRIX_SIZE, sizeof(float), 64);
  float* matrixB = (float*)mkl_calloc(MATRIX_SIZE * MATRIX_SIZE, sizeof(float), 64);
  float* result = (float*)mkl_calloc(MATRIX_SIZE * MATRIX_SIZE, sizeof(float), 64);
  printf("Doing MKL matrix multiplication (%d times) of %d x %d matrices...\n", REPEAT, MATRIX_SIZE, MATRIX_SIZE);
 
  int size = MATRIX_SIZE * MATRIX_SIZE;
  for (int i = 0; i <  size; i++) {
     matrixA[i] = i;
     matrixB[i] = i;	
  }  
  //std::shuffle(
  std::random_shuffle(&matrixA[0], &matrixA[size]);
  std::random_shuffle(&matrixB[0], &matrixB[size]);

  int thr[] = {120, 240};
  for (int t = 0; t < 3; t++) {
	  omp_set_num_threads(thr[t]);
	  printf("Test with %d threads\n", thr[t]);
	  TimerRegister tr;
  	  Timer timer(tr, "Matrix multiplication timer");
	  for (int i = 0; i < REPEAT; i++) {
		  timer.start();
		  matrix_multiply(matrixA, MATRIX_SIZE, MATRIX_SIZE,
		    matrixB, MATRIX_SIZE, MATRIX_SIZE, result);
		  timer.stop();
	  }
      tr.print_results(std::cout);
  }

  mkl_free(matrixA);
  mkl_free(matrixB);
  mkl_free(result);
    
  return 0;
}

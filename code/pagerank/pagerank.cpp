// Parallel Pagerank implementation
// CSR graph representation

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include <math.h>
#include <omp.h>

#include "../Timer.hpp"
#include "graph.hpp"

void pagerank (graph_t *G, int max_iter, float scale_factor) {
  int num_vertices = G->n_vertices;
  float *prev_rank = new float[num_vertices];
  float *rank = new float[num_vertices];
  float global_add = (1.0 - scale_factor) / (float)num_vertices;

  /* Set to initial value */
  float initial = 1.0 / (float)num_vertices;

  for (int i = 0; i < num_vertices; i++) {
    rank[i] = initial;
  }

  int iteration = 0;
  while (iteration++ < max_iter) {
    // Save the previous pagerank values
    #pragma omp parallel for shared(prev_rank, rank)
    for (int i = 0; i < num_vertices; i++) {
        prev_rank[i] = rank[i];
        rank[i] = 0.0;
    }

    float dangling_sum = 0.0f;
    float disconnected_add;
    #pragma omp parallel for reduction(+:dangling_sum)
    for(int i = 0; i < num_vertices; i++) {
      unsigned int start = G->first_neighbor_indices[i];
      unsigned int end = G->first_neighbor_indices[i + 1];
      int num_outgoing_edges = end - start;
      if (num_outgoing_edges == 0) {
          dangling_sum += prev_rank[i];
      }
    }
    disconnected_add = scale_factor * dangling_sum / (float)num_vertices;

    #pragma omp parallel for
    for(int i = 0; i < num_vertices; i++) {

      unsigned int start = G->first_neighbor_indices[i];
      unsigned int end = G->first_neighbor_indices[i + 1];
      int num_outgoing_edges = end - start;

      if (num_outgoing_edges > 0) {
        // http://www.cs.cornell.edu/home/kleinber/networks-book/networks-book-ch14.pdf
        // Each vertex divides its current PageRank equally
        // across its out-going links
        float distribute_value = scale_factor * prev_rank[i] / num_outgoing_edges;
        for (unsigned int e = start; e < end; e++) {
          unsigned int vertex_index = G->neighbors[e];
          //printf("Thread id %d\n", omp_get_thread_num());

          #pragma omp atomic
          rank[vertex_index] += distribute_value;
        }
      }

      #pragma omp atomic
      rank[i] += global_add + disconnected_add;
    }
  }

 /*
  float sum = 0;
  for(int i = 0; i < num_vertices; i++) {
    std::cout << "(" << i << ", " << rank[i] << ")\n";
    sum += rank[i];
  }
  std::cout << "PageRank sum: " << sum << "\n";
*/
  
}

int main (int argc, char* argv[]) {
  if (argc != 2) {
    std::cout << "usage:  pagerank <filename>\n";
    return 0;
  }

  std::cout << "Reading from file " << argv[1] << " ...\n";
  graph_t* g = csr_graph_from_file (argv[1]);
  std::cout << "Graph contains " << g->n_edges << " edges, " << g->n_vertices << " vertices\n";
  std::cout << "--------------------------\n";

  int thr[] ={240}; 
  for (int t = 0; t < 1; t++) {
    TimerRegister tr;
    Timer timer(tr, "Pagerank timer (ran 10x)");
    printf("Set number of threads to %d\n", thr[t]);
    omp_set_num_threads(thr[t]);
    for (int i = 0; i < 30; i++) {
      timer.start();
      pagerank(g, 10, 0.85f);
      timer.stop();
    }
    tr.print_results(std::cout);

  }

  free(g);

  return 0;
}

import matplotlib.pyplot as plt
from pylab import rcParams
rcParams['figure.figsize'] = 10, 6

LABEL_TIME = "Execution time (ms)"
LABEL_THREADS = "Number of threads"

THREADS = [1, 2, 4, 8, 12, 16, 24, 60, 120, 180, 240]


# BFS on ALL nodes as start vertex
# rmat13.txt
# Average of 10 runs
# Time in milliseconds

# Xeon CPU, 24 cores
TIME_BFS = [
	(6168, 0.78),
	(4031, 3),
	(2248, 2),
	(1438, 0.3),
	(1291, 15),
	(2873, 846),
	(4812, 215),
	(6998, 397),
	(9724, 67),
	(15286, 1635),
	(16136, 419)
]

TIME_BFS_PHI = [
	(29688, 8.58),
	(18872, 0.4),
	(11706, 577),
	(7697, 2.2),
	(6352, 1849),
	(5535, 33),
	(4723, 8),
	(4033, 24),
	(4013, 36),
	(4188, 47),
	(4524, 48)
]

def plot_bfs_perf():
	plt.xlim(1, 240)
	plt.xlabel(LABEL_THREADS, fontsize=20)
	plt.ylabel(LABEL_TIME, fontsize=20)

	plt.errorbar(THREADS, [i[0] for i in TIME_BFS], yerr=[i[1] for i in TIME_BFS], marker="o", label="Xeon X5650x2")
	plt.errorbar(THREADS, [i[0] for i in TIME_BFS_PHI], yerr=[i[1] for i in TIME_BFS_PHI], marker="o", label="Xeon Phi 5110P")

	plt.legend(loc='best')
	
	plt.savefig("bfs.png", bbox_inches="tight", dpi=300);

if __name__ == "__main__":
	plot_bfs_perf()

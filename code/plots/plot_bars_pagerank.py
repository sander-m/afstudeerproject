import matplotlib.pyplot as plt
import numpy as np
from pylab import rcParams
rcParams['figure.figsize'] = 10, 6


LABEL_TIME = "Execution time (ms)"

# Pagerank on real-world graphs
# Average of 10 runs, with standard deviation
# Time in milliseconds

# Xeon CPU, 12 cores

# 12 threads
TIME_PAGERANK =  [
	(633.8, 9.75), #as-skitter 24 threads
	(282.4, 4), #web-google
	(127.6, 2.9), #web-stanford
	(129.7, 34.5), #amazon0505
	(799, 76) #wiki-Talk
]

# Xeon Phi
# 240 threads
TIME_PAGERANK_PHI = [
	(2333.6, 39), #as skitter
	(612.6, 101.6), #web-google
	(785.68, 96.5), #web-Stanford
	(356, 117), #amazon0505
	(7860, 94) #wiki-Talk
]

def normalize(arr1, arr2):
	l = len(arr1)
	for i in range(0, l):
		if arr1[i] < arr2[i]:
			arr1[i] = arr1[i] / arr2[i]
			arr2[i] = 1.0
		else:
			arr2[i] = arr2[i] / arr1[i]
			arr1[i] = 1.0

def plot_pagerank_perf():
	ind = np.arange(len(TIME_PAGERANK))

	width = 0.35       # the width of the bars
	fig, ax = plt.subplots()
	means = [i[0] for i in TIME_PAGERANK]
	#std = [i[1] for i in TIME_PAGERANK]

	means_phi = [i[0] for i in TIME_PAGERANK_PHI]
	#std_phi = [i[1] for i in TIME_PAGERANK_PHI]

	normalize(means, means_phi)

	rects1 = ax.bar(ind, means, width, color='r')
	rects2 = ax.bar(ind+width, means_phi, width, color='y')

	ax.set_ylabel('Normalized execution time')
	#ax.set_title('PageRank: Xeon vs Xeon Phi')
	ax.set_xticks(ind+width)
	ax.set_xticklabels( ('as-skitter', 'web-google', 'web-Stanford', 'amazon0505', 'wiki-Talk') )

	ax.legend( (rects1[0], rects2[0]), ('Xeon X5650x2', 'Xeon Phi 5110P') )

	def autolabel(rects):
	    # attach some text labels
	    for rect in rects:
	        height = rect.get_height()
	        ax.text(rect.get_x()+rect.get_width()/2., 1.05*height, '%.2f'%float(height),
	                ha='center', va='bottom')

	autolabel(rects1)
	#autolabel(rects2)
	#plt.legend(loc='best')
	#plt.show()
	plt.savefig("pagerank_bars.png", bbox_inches="tight", dpi=300);

if __name__ == "__main__":
	plot_pagerank_perf()

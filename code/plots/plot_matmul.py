import matplotlib.pyplot as plt
from pylab import rcParams
rcParams['figure.figsize'] = 10, 6

LABEL_TIME = "Execution time (ms)"
LABEL_THREADS = "Number of threads"

THREADS = [1, 2, 4, 8, 12, 16, 24, 60, 120, 180, 240]


# Matrix multiplication
# 5000x5000 float matrix
# Average of 2 runs
# Time in milliseconds

# Xeon CPU, 24 cores
TIME_MATMUL = [
	13521,
	6868,
	3445,
	1724,
	1722,
	1722,
	1722,
	1722,
	1722,
	1722,
	1722
]

TIME_MATMUL_PHI = [
	18706,
	9543,
	8627,
	4347,
	2897,
	2196,
	1456,
	507,
	350,
	341,
	299
]

def plot_matmul_perf():
	#plt.ylim(0, 20000)
	plt.xlim(1, 240)
	plt.xlabel(LABEL_THREADS, fontsize=20)
	plt.ylabel(LABEL_TIME, fontsize=20)

	plt.plot(THREADS, TIME_MATMUL, marker="o", label="Xeon X5650x2")
	plt.plot(THREADS, TIME_MATMUL_PHI, marker="o", label="Xeon Phi 5110P")

	plt.legend(loc='best')
	#plt.show()
	plt.savefig("matmul.png", bbox_inches="tight", dpi=300);

if __name__ == "__main__":
	plot_matmul_perf()

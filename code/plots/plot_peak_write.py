import matplotlib.pyplot as plt
from pylab import rcParams
rcParams['figure.figsize'] = 10, 6

LABEL_TIME = "Execution time (ms)"
LABEL_THREADS = "Number of threads"

THREADS = [4, 8, 12, 16, 24, 60, 120, 180, 240]

# Average of 10 runs
# Reading 25 000 000 integers
# Time in milliseconds

# Xeon CPU, 24 cores
# (avg, standard deviation)

TIME_PEAK_WRITE_SEQ = [
(169.92, 71.9), 	#4
(130.98, 2.6),	    #8
(125.23, 1.44),  	#12
(129.162, 1.74),	    			#16
(122.75, 2.41),
(115.2, 2.65),
(120.58, 2.98),
(119.86, 1.8),
(119.2, 1.47)
]

TIME_PEAK_WRITE_INTERLEAVED = [
(170.45, 4.027),
(152.94, 0.25),
(194.25, 2.98),
(149.58, 0.3),
(172.7, 6.98),
(183.52, 3.26),
(184.6, 2.88),
(184.73, 7.4),
(183.4, 2.006)
]

TIME_PEAK_WRITE_RANDOM = [
(1942, 2.9),
(1139, 0.324),
(1355, 62.8),
(1135, 2.45),
(1189, 22.74),
(1191.9, 32.55),
(1179, 32.2),
(1164.3, 26.2),
(1161, 15.74)
]


# 10 runs (averages)
# 250 million, about 1 GB
# 
TIME_PEAK_WRITE_SEQ_PHI = [
(1083, 95.8), #4
(527.13, 1.255),	 #8
(352.17, 1.58),    #12
(268.9, 13.69),  #16
(203.1, 2.92),
(82.9, 10.39),
(53.157, 17.35),
(46.59, 19.49),
(45.37, 20.16)
]

TIME_PEAK_WRITE_INTERLEAVED_PHI = [
(1966.32, 11.35),	#4
(934.9, 6.625), #8
(621.6, 18.829),	#12	
(446.87, 1.81),  #16
(293.2, 3.935),
(118.5, 0.91),
(51.6, 0.59),
(46.4, 0.24),
(33.57, 0.24)
]

TIME_PEAK_WRITE_RANDOM_PHI = [
(25554, 19.9),    #4
(12815.5, 30.54),  #8
(8911.47, 42.5), #12
(6473.2, 84.3),   #16
(4431.17, 59.1),
(1783, 0.5),
(934.65, 0.5),
(670.8, 0.45),
(519.7, 0.13)
]

def plot_peak_perf():
	num_integers = 250000000
	num_bits = num_integers * 32
	num_bytes = num_bits / 8
	num_gigabytes = num_bytes / 1000000000

	#plt.xlim(1, 244)
	#plt.ylim(0, 50)
	plt.xlabel(LABEL_THREADS, fontsize=20)
	plt.ylabel("Gigabytes per second", fontsize=20)

	#plt.figure()
	# convert to gigabyte per second
	y1 = []
	y1_error = []
	for time, std in TIME_PEAK_WRITE_SEQ:
		gb_per_sec = num_gigabytes / (time / 1000.0)
		y1.append(gb_per_sec)
		y1_error.append((std / (time + std)) * gb_per_sec)

	y2 = []
	y2_error = []
	for time, std in TIME_PEAK_WRITE_INTERLEAVED:
		gb_per_sec = num_gigabytes / (time / 1000.0)
		y2.append(gb_per_sec)
		y2_error.append((std / (time + std)) * gb_per_sec)

	y3 = []
	y3_error = []
	for time, std in TIME_PEAK_WRITE_RANDOM:
		gb_per_sec = num_gigabytes / (time / 1000.0)
		y3.append(gb_per_sec);
		y3_error.append((std / (time + std)) * gb_per_sec)

	print('-----------------------')
	print("X5650 random write GB/s")
	print(y3[4])
	print("X5650 interleaved write GB/s")
	print(y2[4])
	print("X5650 sequential write GB/s")
	print(y1[4])
	print('-----------------------')

	plt.errorbar(THREADS, y1, yerr=y1_error, label="Xeon X5650x2 write sequential")
	plt.errorbar(THREADS, y2, yerr=y2_error, label="Xeon X5650x2 write interleaved")
	#plt.errorbar(THREADS, y3, yerr=y3_error, label="Xeon X5650x2")

	num_integers = 250000000
	num_bits = num_integers * 32
	num_bytes = num_bits / 8
	num_gigabytes = num_bytes / 1000000000
	y1_phi = []
	y1_error_phi = []
	for time, std in TIME_PEAK_WRITE_SEQ_PHI:
		gb_per_sec = num_gigabytes / (time / 1000.0)
		y1_phi.append(num_gigabytes / (time / 1000.0));
		y1_error_phi.append((std / (time + std)) * gb_per_sec)

	y2_phi = []
	y2_error_phi = []
	for time, std in TIME_PEAK_WRITE_INTERLEAVED_PHI:
		gb_per_sec = num_gigabytes / (time / 1000.0)
		y2_phi.append(num_gigabytes / (time / 1000.0));

	y3_phi = []
	y3_error_phi = []
	for time, std in TIME_PEAK_WRITE_RANDOM_PHI:
		gb_per_sec = num_gigabytes / (time / 1000.0)
		y3_phi.append(num_gigabytes / (time / 1000.0));
		y3_error_phi.append((std / (time + std)) * gb_per_sec)


	print('-----------------------')
	print("Xeon Phi random read GB/s")
	print(y3_phi[8])
	print("Xeon Phi interleaved read GB/s")
	print(y2_phi[8])
	print("Xeon Phi sequential read GB/s")
	print(y1_phi[8])
	print('-----------------------')

	#plt.errorbar(THREADS, y1_phi, yerr=y1_error_phi, marker="o", label="60-core Xeon Phi write sequential")
	plt.xticks(THREADS)
	plt.plot(THREADS, y2_phi, label="Xeon Phi 5110P write interleaved")
	plt.errorbar(THREADS, y1_phi,  yerr=y1_error_phi, marker="o", label="Xeon Phi 5110P write sequential")
	plt.legend(loc='best')
	#plt.show()
	
	plt.savefig("peak_write_seq_interleaved.png", bbox_inches="tight", dpi=300);

if __name__ == "__main__":
	plot_peak_perf()

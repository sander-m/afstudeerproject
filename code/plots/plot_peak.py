import matplotlib.pyplot as plt
from pylab import rcParams
rcParams['figure.figsize'] = 10, 6

LABEL_TIME = "Execution time (ms)"
LABEL_THREADS = "Number of threads"

THREADS = [4, 8, 12, 16, 24, 60, 120, 180, 240]

# Average of 10 runs
# Reading 25 000 000 integers
# Time in milliseconds

# Xeon CPU, 24 cores
# (avg, standard deviation)

TIME_PEAK_READ_SEQ = [
(134.47, 12.2), 	#4
(110.73, 0.75),	    #8
(99.25, 0.216),  	#12
(107.57, 5.67),	    #16
(102.345, 2.774),
(98.93, 2.7),
(99.18, 3.67),
(98.37, 2.9),
(97.824, 1.4)
]

TIME_PEAK_READ_INTERLEAVED = [
(185.4, 0.5),
(139.13, 0.13),
(129.83, 0.28),
(140.37, 5.94),
(149.2, 6.142),
(143.16, 1.56),
(143.53, 2.78),
(143.47, 1.966),
(143.33, 1.357)
]

TIME_PEAK_READ_RANDOM = [
(1117, 13.36),
(920.89, 0.96),
(942.539, 43.26),
(916.98, 22.36),
(866.68, 35.241),
(831.66, 15.48),
(825.716, 12.319),
(820.75, 10.56),
(817.544, 3.508)
]


# 10 runs (averages)
# 250 million, about 1 GB
# 
TIME_PEAK_READ_SEQ_PHI = [
(272.18, 94.96), #4
(129.15, 1.55),	 #8
(82.8, 1.43),    #12
(71.97, 21.24),  #16
(45.69, 3.53),
(22.19, 10.2),
(18.88, 15.58),
(18.98, 19.15),
(19.592, 21.8)
]

TIME_PEAK_READ_INTERLEAVED_PHI = [
(362.48, 0.2),	#4
(194.78, 0.62), #8
(122.78, 0.04),	#12	
(97.71, 0.61),  #16
(66.36, 0.54),
(28.29, 0.11),
(16.6, 0.57),
(19.22, 0.17),
(14.1, 1.183)
]

TIME_PEAK_READ_RANDOM_PHI = [
(24275, 97.3),    #4
(12243, 87.386),  #8
(8074.3, 117.95), #12
(6210.3, 0.37),   #16
(4068, 68),
(1670, 7.5),
(853, 0.2),
(585.4, 4.48),
(443.8, 0.24)
]

def plot_peak_perf():
	num_integers = 250000000
	num_bits = num_integers * 32
	num_bytes = num_bits / 8
	num_gigabytes = num_bytes / 1000000000

	plt.xlim(1, 240)
	#plt.ylim(0, 50)
	plt.xlabel(LABEL_THREADS, fontsize=20)
	plt.ylabel("Gigabytes per second", fontsize=20)

	#plt.figure()
	# convert to gigabyte per second
	y1 = []
	y1_error = []
	for time, std in TIME_PEAK_READ_SEQ:
		gb_per_sec = num_gigabytes / (time / 1000.0)
		y1.append(gb_per_sec)
		y1_error.append((std / (time + std)) * gb_per_sec)

	y2 = []
	y2_error = []
	for time, std in TIME_PEAK_READ_INTERLEAVED:
		gb_per_sec = num_gigabytes / (time / 1000.0)
		y2.append(gb_per_sec)
		y2_error.append((std / (time + std)) * gb_per_sec)

	#y2 = []
	#for i in TIME_PEAK_READ_INTERLEAVED:
	#y2.append(num_gigabytes / (i / 1000.0));

	y3 = []
	y3_error = []
	for time, std in TIME_PEAK_READ_RANDOM:
		gb_per_sec = num_gigabytes / (time / 1000.0)
		y3.append(gb_per_sec);
		y3_error.append((std / (time + std)) * gb_per_sec)

	print('-----------------------')
	print("X5650 random read GB/s")
	print(y3[4])
	print("X5650 interleaved read GB/s")
	print(y2[4])
	print("X5650 sequential read GB/s")
	print(y1[4])
	print('-----------------------')

	#plt.errorbar(THREADS, y1, yerr=y1_error, label="Xeon X5650x2 read seq")
	plt.errorbar(THREADS, y2, yerr=y2_error, label="Xeon X5650x2 read interleaved")
	plt.errorbar(THREADS, y1, yerr=y1_error, label="Xeon X5650x2 read sequential")

	num_integers = 250000000
	num_bits = num_integers * 32
	num_bytes = num_bits / 8
	num_gigabytes = num_bytes / 1000000000
	y1_phi = []
	y1_error_phi = []
	for time, std in TIME_PEAK_READ_SEQ_PHI:
		gb_per_sec = num_gigabytes / (time / 1000.0)
		y1_phi.append(num_gigabytes / (time / 1000.0));
		y1_error_phi.append((std / (time + std)) * gb_per_sec)

	y2_phi = []
	y2_error_phi = []
	for time, std in TIME_PEAK_READ_INTERLEAVED_PHI:
		gb_per_sec = num_gigabytes / (time / 1000.0)
		y2_phi.append(num_gigabytes / (time / 1000.0));
		y2_error_phi.append((std / (time + std)) * gb_per_sec)

	y3_phi = []
	y3_error_phi = []
	for time, std in TIME_PEAK_READ_RANDOM_PHI:
		gb_per_sec = num_gigabytes / (time / 1000.0)
		y3_phi.append(num_gigabytes / (time / 1000.0));
		y3_error_phi.append((std / (time + std)) * gb_per_sec)

	print('-----------------------')
	print("Xeon Phi random read GB/s")
	print(y3_phi[8])
	print("Xeon Phi interleaved read GB/s")
	print(y2_phi[8])
	print("Xeon Phi sequential read GB/s")
	print(y1_phi[8])
	print('-----------------------')


	plt.errorbar(THREADS, y1_phi, yerr=y1_error_phi, marker="o", label="Xeon Phi 5110P read sequential")
	plt.errorbar(THREADS, y2_phi, yerr=y2_error_phi, label="Xeon Phi 5110P read interleaved")
	#plt.errorbar(THREADS, y3_phi,  yerr=y3_error_phi, marker="o", label="Xeon Phi 5110P")
	plt.legend(loc='upper left')
	#plt.show()
	
	plt.savefig("peak_read_seq_interleaved.png", bbox_inches="tight", dpi=300);

if __name__ == "__main__":
	plot_peak_perf()

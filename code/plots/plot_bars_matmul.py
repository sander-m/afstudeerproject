import matplotlib.pyplot as plt
from pylab import rcParams
import numpy as np

rcParams['figure.figsize'] = 10, 6

LABEL_TIME = "Execution time (ms)"

# BFS on ALL nodes as start vertex
# Average of 10 runs
# Time in milliseconds
# BFS starting from node 0 up to 10

TIME_MATMUL = [
	(501.57, 269), #3000x3000
	(1725, 0), # 5000x5000
	(4730, 0.95) #7000x7000
]

TIME_MATMUL_PHI = [
	(93.7, 58.2), #3000x3000
	(374, 92.7), #5000x5000
	(961.4, 45) #7000x7000 120 threads
]

def plot_matmul_perf():
	ind = np.arange(len(TIME_MATMUL_PHI))
	width = 0.35       # the width of the bars
	fig, ax = plt.subplots()
	means = [i[0] for i in TIME_MATMUL]
	std = [i[1] for i in TIME_MATMUL]

	means_phi = [i[0] for i in TIME_MATMUL_PHI]
	std_phi = [i[1] for i in TIME_MATMUL_PHI]

	rects1 = ax.bar(ind, means, width, color='r', yerr=std)
	rects2 = ax.bar(ind+width, means_phi, width, color='y', yerr=std_phi)


	ax.set_ylabel('Execution time (ms)')
	ax.set_xlabel('Matrix size')
	#ax.set_title('BFS: Xeon vs Xeon Phi')
	ax.set_xticks(ind+width)
	ax.set_xticklabels( ('3000x3000', '5000x5000', '7000x7000'))

	ax.legend( (rects1[0], rects2[0]), ('Xeon X5650x2', 'Xeon Phi 5110P'), loc='upper left')

	def autolabel(rects):
	    # attach some text labels
	    for rect in rects:
	        height = rect.get_height()
	        ax.text(rect.get_x()+rect.get_width()/2., 1.05*height, '%d'%int(height),
	                ha='center', va='bottom')

	autolabel(rects1)
	autolabel(rects2)
	#plt.legend(loc='best')
	#plt.show()
	plt.savefig("matmul_bars.png", bbox_inches="tight", dpi=300);

if __name__ == "__main__":
	plot_matmul_perf()

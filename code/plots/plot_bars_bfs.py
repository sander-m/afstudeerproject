import matplotlib.pyplot as plt
from pylab import rcParams
import numpy as np

rcParams['figure.figsize'] = 10, 6

LABEL_TIME = "Execution time (ms)"

# Average of 10 runs
# Time in milliseconds
# BFS starting from node 0 up to 10
""""
Fixed threshold = 200
TIME_BFS = [
	(1253, 13.4),   #as-skitter
	(2251, 26.5),   #web-google
	(656, 3.1),     #web-stanford, 12 threads
	(1347, 5.2),    #amazon
	(325.24, 0.13)  #wiki-Talk
]

TIME_BFS_PHI = [
	(1235, 92.5),    #as-skitter
	(4845, 50.4),    #web-google
	(3716, 268),     #web-stanford
	(1104, 93.4),    #amazon
	(282.16, 92.85)  #wiki-talk
]

# Graphs with losw average and low max degree
TIME_BFS_ROADNET = [
	(76114, 34),     #roadNet-CA  
	(43474, 75.4),   #roadNet-PA
	(67600, 138),	 #roadNet-TX
	(143573, 7446),	  #degree6_100 
	(67435, 541),  # mesh 1000x1000 nodes 0 t/m 100
	#(7546.3, 53.7)   #inf-italy-osm
]

TIME_BFS_ROADNET_PHI = [
	(54000, 260),     #roadNet-CA
	(34564, 193),     #roadNet-PA
	(48244, 49.6),	  #roadNet-TX
	(132835, 184),	   #degree6_100
	(122893, 2934),    #mesh 1000x1000 nodes 0 t/m4
	()
	#(3026.9, 112.6)   #inf-italy-osm
]
"""

""" Adjusted threshold """
## NEW TESTS with different threshold (1% of n_vertices)
# Graphs with losw average and low max degree

TIME_BFS = [
	(8674, 135),     #as-skitter
	(8779.9, 105),   #web-google
	(5460.834, 105), #web-stanford
	(8046, 97.5),    #amazon
	(4123.26, 87.7)  #wiki-Talk
]

TIME_BFS_PHI = [
	(9580.4, 162),    #as-skitter
	(5640, 159),      #web-google
	(4407, 152),      #web-stanford
	(5276.4, 169),    #amazon
	(12595.74, 157)   #wiki-talk
]


TIME_BFS_ROADNET = [
	(34442, 340.7),     #roadNet-CA  
	(22176, 292.1),   	#roadNet-PA
	(32834, 259),	 	#roadNet-TX
	(61857, 153.14),	#degree6_100 
	(67982, 758)  		#mesh 1000x1000 nodes 0 t/m 100
]

TIME_BFS_ROADNET_PHI = [
	(20221.85, 150.3),     # roadNet-CA
	(13207, 171.5),    	   # roadNet-PA
	(19067, 157),	  	   # roadNet-TX
	(51358.3, 103.54),	   # degree6_100
	(34562.87, 137.94)     # mesh 1000x1000 nodes 0 t/m4
]

def normalize(arr1, arr2):
	l = len(arr1)
	for i in range(0, l):
		if arr1[i] < arr2[i]:
			arr1[i] = float(arr1[i]) / arr2[i]
			arr2[i] = 1.0
		else:
			arr2[i] = float(arr2[i]) / arr1[i]
			arr1[i] = 1.0

def plot_bfs_perf_roadnet():
	ind = np.arange(len(TIME_BFS_ROADNET))
	width = 0.35       # the width of the bars
	fig, ax = plt.subplots()
	means = [i[0] for i in TIME_BFS_ROADNET]
	#std = [i[1] for i in TIME_BFS_ROADNET]

	means_phi = [i[0] for i in TIME_BFS_ROADNET_PHI]
	#std_phi = [i[1] for i in TIME_BFS_ROADNET_PHI]
	#normalize(means_phi, means)

	rects1 = ax.bar(ind, means, width, color='r')
	rects2 = ax.bar(ind+width, means_phi, width, color='y')

	ax.set_ylabel('Execution time (ms)')
	#ax.set_title('BFS: Xeon vs Xeon Phi')
	ax.set_xticks(ind+width)
	ax.set_xticklabels( ('roadNet-CA', 'roadNet-PA', 'roadNet-TX', "degree6_1M", 'mesh1000x1000') )

	ax.legend( (rects1[0], rects2[0]), ('Xeon X5650x2', 'Xeon Phi 5110P'), loc='upper left')

	def autolabel(rects):
	    # attach some text labels
	    for rect in rects:
	        height = rect.get_height()
	        ax.text(rect.get_x()+rect.get_width()/2., 1.05*height, '%.2f'%float(height),
	                ha='center', va='bottom')

	#autolabel(rects1)
	#autolabel(rects2)
	#plt.legend(loc='best')
	#plt.show()
	plt.savefig("bfs_roadnet_bars.png", bbox_inches="tight", dpi=300);

def plot_bfs_perf():
	ind = np.arange(len(TIME_BFS_PHI))
	width = 0.35       # the width of the bars
	fig, ax = plt.subplots()
	means = [i[0] for i in TIME_BFS]
	std = [i[1] for i in TIME_BFS]

	means_phi = [i[0] for i in TIME_BFS_PHI]
	std_phi = [i[1] for i in TIME_BFS_PHI]

	#normalize(means_phi, means)

	rects1 = ax.bar(ind, means, width, color='r')
	rects2 = ax.bar(ind+width, means_phi, width, color='y')

	ax.set_ylabel('Execution time (ms)')
	#ax.set_title('BFS: Xeon vs Xeon Phi')
	ax.set_xticks(ind+width)
	ax.set_xticklabels( ('as-skitter', 'web-google', 'web-Stanford', 'amazon0505', 'wiki-Talk') )

	ax.legend( (rects1[0], rects2[0]), ('Xeon X5650x2', 'Xeon Phi 5110P'), loc='upper left')

	def autolabel(rects):
	    # attach some text labels
	    for rect in rects:
	        height = rect.get_height()
	        ax.text(rect.get_x()+rect.get_width()/2., 1.05*height, '%.2f'%float(height),
	                ha='center', va='bottom')

	#autolabel(rects1)
	#autolabel(rects2)
	#plt.legend(loc='best')
	#plt.show()
	plt.savefig("bfs_bars.png", bbox_inches="tight", dpi=300);

if __name__ == "__main__":
	plot_bfs_perf()
	plot_bfs_perf_roadnet()

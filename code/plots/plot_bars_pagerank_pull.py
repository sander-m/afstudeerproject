import matplotlib.pyplot as plt
import numpy as np
from pylab import rcParams
rcParams['figure.figsize'] = 10, 6


LABEL_TIME = "Execution time (ms)"

# Pagerank on real-wrorld graphs
# Average of 10 runs, with standard deviation
# Time in milliseconds

# Xeon CPU, 12 cores

# 12 threads
TIME_PAGERANK_PULL =  [
	(87.6, 2.10),  #as-skitter
	(63.4, 14.17), #web-google
	(17.25, 1.4),  #web-stanford
	(28.56, 2.34), #amazon0505
	(89.15, 2.6)   #wiki-Talk
	#(63.4, 2.9),  #roadNet-CA
	#(28.97, 1.3), #roadNet-PA
	#(42.7, 2.4)   #roadNet-TX
]

# Xeon Phi
# 240 threads
TIME_PAGERANK_PULL_PHI = [
	(127.56, 43.7),  #as skitter
	(159.7, 43.05),  #web-google
	(100.7, 40.86),  #web-Stanford
	(102.517, 40.6), #amazon0505
	(373.4, 44.25)   #wiki-Talk
	#(69.87, 16.8),  #roadNet-CA
	#(56.67, 44.5),  #roadNet-PA
	#(66.6, 68)	     #roadNet-TX
]

def normalize(arr1, arr2):
	l = len(arr1)
	for i in range(0, l):
		if arr1[i] < arr2[i]:
			arr1[i] = arr1[i] / arr2[i]
			arr2[i] = 1.0
		else:
			arr2[i] = arr2[i] / arr1[i]
			arr1[i] = 1.0

def plot_pagerank_perf():
	ind = np.arange(len(TIME_PAGERANK_PULL))
	width = 0.35 # the width of the bars
	fig, ax = plt.subplots()
	means = [i[0] for i in TIME_PAGERANK_PULL]
	std = [i[1] for i in TIME_PAGERANK_PULL]

	means_phi = [i[0] for i in TIME_PAGERANK_PULL_PHI]
	std_phi = [i[1] for i in TIME_PAGERANK_PULL_PHI]

	normalize(means, means_phi)

	rects1 = ax.bar(ind, means, width, color='r')
	rects2 = ax.bar(ind+width, means_phi, width, color='y')

	ax.set_ylabel('Normalized execution time')
	#ax.set_title('Pull-based PageRank: Xeon vs Xeon Phi')
	ax.set_xticks(ind+width)
	ax.set_xticklabels( ('as-skitter', 'web-google', 'web-Stanford', 'amazon0505', 'wiki-Talk') )

	ax.legend( (rects1[0], rects2[0]), ('Xeon X5650x2', 'Xeon Phi 5110P') )

	def autolabel(rects):
	    # attach some text labels
	    for rect in rects:
	        height = rect.get_height()
	        ax.text(rect.get_x()+rect.get_width()/2., 1.05*height, '%.2f'%float(height),
	                ha='center', va='bottom')

	autolabel(rects1)
	#autolabel(rects2)
	#plt.legend(loc='best')
	#plt.show()
	plt.savefig("pagerank_pull_bars.png", bbox_inches="tight", dpi=300);

if __name__ == "__main__":
	plot_pagerank_perf()

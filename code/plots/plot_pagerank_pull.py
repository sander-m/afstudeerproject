import matplotlib.pyplot as plt
from pylab import rcParams
rcParams['figure.figsize'] = 10, 6


LABEL_TIME = "Execution time (ms)"
LABEL_THREADS = "Number of threads"

THREADS = [1, 2, 4, 8, 12, 16, 24, 60, 120, 180, 240]


# Pagerank on random50.txt
# Average of 10 runs
# Time in milliseconds

# Xeon CPU, 24 cores
TIME_PAGERANK =  [
	(339, 0.68),
	(341, 4.0),
	(300, 1.6),
	(195, 0.1),
	(200.8, 9.6),
	(205, 48.1),
	(122.9, 5.08),
	(117.5, 2.8),
	(112, 3.08),
	(112, 2.26),
	(113, 1.94)
]

TIME_PAGERANK_PHI = [
	(1998, 8.85),
	(2449, 11.35),
	(2823, 4.77),
	(2216, 6.24),
	(1715, 3.32),
	(1397, 19.5),
	(985, 2.62),
	(443, 11.17),
	(239, 15.77),
	(170, 19.98),
	(139, 21.3)
]


# Random 50_2
TIME_PAGERANK_PULL =  [
	(195.8, 5.6),
	(146.7, 2.8),
	(88.8, 1.9),
	(51.4, 4.98),
	(62.41, 0.7),
	(67.84, 12.7),
	(39.98, 2.6),
	(35.3, 1.66),
	(37.7, 5.59),
	(37, 2.381),
	(39, 1.370)
]

TIME_PAGERANK_PULL_PHI = [
	(2181, 8.2),
	(1716, 7.09),
	(1031, 11.3),
	(565.8, 5.68),
	(388.69, 3.8),
	(304.27, 15.6),
	(200.84, 5.9),
	(89.3, 10.5),
	(55.89, 15.54),
	(43.79, 19.8),
	(38.1, 20.8)
]



def plot_pagerank_perf():
	plt.xlim(1, 240)
	plt.xlabel(LABEL_THREADS, fontsize=20)
	plt.ylabel(LABEL_TIME, fontsize=20)

	plt.errorbar(THREADS, [i[0] for i in TIME_PAGERANK_PULL], yerr=[i[1] for i in TIME_PAGERANK_PULL], marker="o", label="Xeon X5650x2")
	plt.errorbar(THREADS, [i[0] for i in TIME_PAGERANK_PULL_PHI], yerr=[i[1] for i in TIME_PAGERANK_PULL_PHI], marker="o", label="Xeon Phi 5110P")

	plt.legend(loc='best')
	#plt.show()
	plt.savefig("pagerank_pull.png", bbox_inches="tight", dpi=300);

if __name__ == "__main__":
	plot_pagerank_perf()

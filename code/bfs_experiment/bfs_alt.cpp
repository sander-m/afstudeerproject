// Breadth first search implementation
// Uses CSR representation

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <algorithm> 

#include <math.h>
#include <omp.h>

#include "../Timer.hpp"
#include "graph.hpp"

static const int DEBUG = 0;
static const uint8_t VISITED = 1;
static const uint8_t UNVISITED = 0;

void bfs(int start_vertex_idx, graph_t *G, std::vector<unsigned int> *level_counts) {
  bool updated = true;
  unsigned int current_level = 0;
  G->vertices[start_vertex_idx].level = 1;
  unsigned int level_count = 1;

  while (updated) {
    if (level_counts != NULL) {
      level_counts->push_back(level_count);
    }

    updated = false;
    current_level++;
    level_count = 0;

    #pragma omp parallel for reduction(+:level_count)
    for (int i = 0; i < G->n_edges; i++) {
      if (G->edges_visited[i] == UNVISITED) {
        unsigned int origin = (*G->edges)[i].first; 
       
        if (G->vertices[origin].level == current_level) {
          G->edges_visited[i] = VISITED;
          unsigned int dest = (*G->edges)[i].second;
          unsigned int dest_level = G->vertices[dest].level;
          if (G->vertices[dest].level == 0) {
            G->vertices[dest].level = current_level + 1;
            level_count++;
            updated = true;
          }
          //printf("Setting level\n");
        }
      }
    }
    //printf("Iteration\n");
  }

}

void benchmark_bfs(graph_t *g) {
  TimerRegister tr;
  Timer timer(tr, "BFS timer (BFS executed for all nodes)");

  TimerRegister tr2;
  Timer timer2(tr2, "Reset timer");

  std::vector<unsigned int> level_counts;

  for (int start = 0; start < g->n_vertices; start++) { 
     /* Clear all */
     #pragma omp parallel for 
     for (int v = 0; v < g->n_vertices; v++) {
        g->vertices[v].level = 0;
     }
     #pragma omp parallel for 
     for (int e = 0; e < g->n_edges; e++) {
        g->edges_visited[e] = UNVISITED;
     }
     level_counts.clear();
     
     timer.start();
     bfs(start, g, &level_counts);
     timer.stop();
  }

  /*
  int sum = 0;
  for (int i = 0; i < level_counts.size(); i++) {
    printf("Level %d contains %d nodes\n", i + 1, (int)level_counts[i]);
    sum += level_counts[i];
  }
  printf("Levels: %d\n", (int)level_counts.size());
  printf("Total nodes: %d\n", sum);
  */

  tr.print_results(std::cout);
}

int main (int argc, char* argv[]) {
  std::cout << "Reading from file " << argv[1] << " ...\n";
  graph_t *g = csr_graph_from_file (argv[1]);
  g->edges_visited = (uint8_t*) calloc(g->n_edges, sizeof(uint8_t));
  
  printf("%d edges, %d vertices\n", g->n_edges, g->n_vertices);
  std::cout << "--------------------------\n";
  
  printf("Starting hybrid-BFS benchmark\n");
  
  int thr[] = {1, 2, 4, 8, 12, 16, 24, 60, 120, 180, 240};
  for (int i = 0; i < 11; i++) {
    printf("Num threads set to %d\n", thr[i]);
    omp_set_num_threads(thr[i]);
    benchmark_bfs(g);
  }

  free(g->vertices);


  return 0;
}

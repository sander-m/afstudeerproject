
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <algorithm>
#include <math.h>
#include <omp.h>

#include "Timer.hpp"

static const int USE_OPENMP = 1;
static const int num_ints = 250000000;
// 32-bit ints, equals about 1 GB

int read_array(int32_t *arr, int32_t *indices, int num_int) {
  unsigned int val = 0;

  #pragma omp parallel for if (USE_OPENMP == 1)
  for (int i = 0; i < num_int; i++) {
    val += arr[indices[i]];
  }

  return val;
}

int write_array(int32_t *arr, int32_t *indices, int num_int) {

  #pragma omp parallel for if (USE_OPENMP == 1)
  for (int i = 0; i < num_int; i++) {
    arr[indices[i]] = i;
  }
  return 0;
}

static void benchmark(int32_t* arr, int32_t *indices, int32_t *rand_indices) {
  TimerRegister tr;
  Timer timer(tr, "Timer sequential");
  printf("Starting memory benchmark, %d integers\n", num_ints);
  for (int i = 0; i < num_ints; i++) {
    indices[i] = i;
  }

  /* Read sequentially */
  printf("Writing with sequential indices (10x) ...\n");
  for (int i = 0; i < 10; i++) {
    timer.start();
    int res = write_array(arr, indices, num_ints);
    timer.stop();
  }
  tr.print_results(std::cout);

  /* Read interleaved */
  printf("Writing with interleaved indices (odd-even) (10x)...\n");
  int index = 0;
  for (int i = 0; index < num_ints / 2; i += 2) {
      indices[index++] = i;
  }
  for (int i = 1; index < num_ints; i += 2) {
      indices[index++] = i;
  }
  
  TimerRegister tr2;
  Timer timer2(tr2, "Timer interleaved");
  
  for (int i = 0; i < 10; i++) {
    timer2.start();
    int res = write_array(arr, indices, num_ints);
    timer2.stop();
  }
  tr2.print_results(std::cout);

  /* Read with random indices */
  printf("Writing with random indices... (10x)\n");
  TimerRegister tr3;
  Timer timer3(tr3, "Timer random");

  for (int i = 0; i < 10; i++) {
    timer3.start();
    int res = write_array(arr, rand_indices, num_ints);
    timer3.stop();
  }
  tr3.print_results(std::cout);
  printf("\n");
}

int main (int argc, char* argv[]) {
  printf("Started\n");

   // Allocate array and arrays for indices
  int32_t *arr = (int32_t*) malloc(num_ints * sizeof(int32_t));
  int32_t *indices = (int32_t*) malloc(num_ints * sizeof(int32_t));
  int32_t *rand_indices = (int32_t*) malloc(num_ints * sizeof(int32_t));
  for (int i = 0; i < num_ints; i++) {
    rand_indices[i] = i;
  }
  std::random_shuffle(&rand_indices[0], &rand_indices[num_ints]);
  
  int thr[] = {4, 8, 12, 16, 24, 60, 120, 180, 240};
  for (int i = 0; i < 9; i++) {
    omp_set_num_threads(thr[i]);
    printf("-------------- Peak test with %d threads --------------\n", thr[i]);
    benchmark(arr, indices, rand_indices);
    printf("-------------- end of test ----------------------------\n");
  }
  free(arr);
  free(indices);

  return 0;
}





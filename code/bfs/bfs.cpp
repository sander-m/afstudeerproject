// Breadth first search implementation
// Uses CSR representation

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include <math.h>
#include <omp.h>

#include "../Timer.hpp"
#include "graph.hpp"

static const int DEBUG = 0;

void bfs_step_top_down(graph_t *G, int current_step) {
  unsigned int frontier_size = 0;

  #pragma omp parallel for reduction(+:frontier_size)
  for (int i = 0; i < G->n_vertices; i++) {
    if (G->vertices[i].level == current_step) {
      /* Visit and add neighbors to the next frontier */
      unsigned int start = G->first_neighbor_indices[i];
      unsigned int end = G->first_neighbor_indices[i + 1];

      for (int e = start; e < end; e++) {
        unsigned int neighbor_index = G->neighbors[e];
        vertex_t *neighbor = &G->vertices[neighbor_index];
        if (!neighbor->visited.exchange(true)) {
          neighbor->level = current_step + 1;
          frontier_size++;
        }
      }
    }
  }
  G->frontier_size = frontier_size;
}

void bfs_step_bottom_up(graph_t *G, int current_step) {
  unsigned int frontier_size = 0;

  #pragma omp parallel for reduction(+:frontier_size)
  for (int i = 0; i < G->n_vertices; i++) {
    if (!G->vertices[i].visited) {
      // Find parent in frontier
      unsigned int start = G->first_parent_indices[i];
      unsigned int end = G->first_parent_indices[i + 1];

      for (int e = start; e < end; e++) {
        unsigned int parent_index = G->parents[e];
        vertex_t *parent = &G->vertices[parent_index];
        if (parent->visited && parent->level == current_step)   {
          G->vertices[i].visited = true;
          G->vertices[i].level = current_step + 1;
          frontier_size++;
          break;
        }
      }
    }
  }
  G->frontier_size = frontier_size;
}

void bfs(int start_vertex_idx, graph_t *G, std::vector<unsigned int> *level_counts) {
  vertex_t *start_vertex = &G->vertices[start_vertex_idx];
  start_vertex->level = 1;
  start_vertex->visited = true;
  G->frontier_size = 1;
  int step = 0;
	
  float bottom_up_threshold = 0.01 * G->n_vertices;
  while (G->frontier_size > 0) {
    step++;
    if (level_counts != NULL) {
      level_counts->push_back(G->frontier_size);
    }
    if (G->frontier_size > bottom_up_threshold) {
      //printf("Step %d: bottom up\n", step);
      bfs_step_bottom_up(G, step);
    } else {
      //printf("Step %d: top down\n", step);
      bfs_step_top_down(G, step);
    }
  }

}

void benchmark_bfs(graph_t *g) {
  TimerRegister tr;
  Timer timer(tr, "BFS timer (BFS executed for all nodes)");
  std::vector<unsigned int> level_counts;
  // Go through all vertices
  for (int repeat = 0; repeat < 5; repeat++) {
  timer.start();
  for (int start = 0; start < 100; start++) {
   
   #pragma omp parallel for 
   for (int v = 0; v < g->n_vertices; v++) {
      g->vertices[v].level = 0;
      g->vertices[v].visited = false;
   }
   level_counts.clear();
   bfs (start, g, &level_counts);

   if (DEBUG == 1) {
     int sum = 0;
     for (int i = 0; i < level_counts.size(); i++) {
      printf("Level %d contains %d nodes\n", i + 1, (int)level_counts[i]);
      sum += level_counts[i];
     }
     printf("Levels: %d\n", (int)level_counts.size());
     printf("Total nodes: %d\n", sum);
   }
  }
  timer.stop();
  printf("- Finished round %d\n", repeat);
  }
  tr.print_results(std::cout);
}

int main (int argc, char* argv[]) {
  std::cout << "Reading from file " << argv[1] << " ...\n";
  graph_t *g = csr_graph_from_file (argv[1]);
  
  printf("%d edges, %d vertices\n", g->n_edges, g->n_vertices);
  std::cout << "--------------------------\n";
  
  printf("Starting hybrid-BFS benchmark\n");
  
  int thr[] = {24};
  for (int i = 0; i < 1; i++) {
    printf("Num threads set to %d\n", thr[i]);
    omp_set_num_threads(thr[i]);
    benchmark_bfs(g);
  }

  free(g->vertices);


  return 0;
}

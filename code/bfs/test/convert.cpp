#include <cstring>
#include <string>
#include <iostream>
#include <fstream>

#include <map>
#include "GraphFile.hpp"

int main(int argc, char **argv) {
    if (argc < 2) {
        std::cout << "Missing graph argument\n";
        return 0;
    }
    std::string filename = std::string(argv[1]);

    std::ofstream outfile;
    outfile.open (filename + "_converted.txt");
    const GraphFile graph(filename);

    std::cout << "#vertices: " << graph.vertex_count << "\n";
    std::cout << "#edges: " << graph.edge_count << "\n";
    for (int j = 0; j < graph.vertex_count; j++) {
        int start = graph.vertices[j];
        int end = graph.vertices[j + 1];
        for (int e = start; e < end; e++) {
            outfile << j << " " << graph.edges[e] << "\n";
        }
    }
    outfile.close();

    return 0;
}

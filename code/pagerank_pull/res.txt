Reading from file random50.mic ...
Graph contains 2500141 edges, 50000 vertices
--------------------------
Set number of threads to 1
Timer results with precision: 1000000

Pagerank timer (ran 10x) (10): 
Min:   1 s   982 ms   120 us     0 ns
Avg:   1 s   992 ms   566 us   200 ns
Max:   2 s     5 ms   896 us     0 ns
Std:   0 s     9 ms   138 us   506 ns

Set number of threads to 2
Timer results with precision: 1000000

Pagerank timer (ran 10x) (10): 
Min:   2 s   343 ms    90 us     0 ns
Avg:   2 s   349 ms   591 us   300 ns
Max:   2 s   357 ms   814 us     0 ns
Std:   0 s     4 ms   763 us   311 ns

Set number of threads to 4
Timer results with precision: 1000000

Pagerank timer (ran 10x) (10): 
Min:   2 s   820 ms   506 us     0 ns
Avg:   2 s   822 ms   268 us   800 ns
Max:   2 s   823 ms   847 us     0 ns
Std:   0 s     1 ms    75 us   498 ns

Set number of threads to 8
Timer results with precision: 1000000

Pagerank timer (ran 10x) (10): 
Min:   2 s   196 ms   693 us     0 ns
Avg:   2 s   198 ms   367 us   100 ns
Max:   2 s   199 ms   215 us     0 ns
Std:   0 s     0 ms   696 us   415 ns

Set number of threads to 16
Timer results with precision: 1000000

Pagerank timer (ran 10x) (10): 
Min:   1 s   364 ms   105 us     0 ns
Avg:   1 s   373 ms   798 us   200 ns
Max:   1 s   421 ms   325 us     0 ns
Std:   0 s    17 ms    97 us   288 ns

Set number of threads to 32
Timer results with precision: 1000000

Pagerank timer (ran 10x) (10): 
Min: 769 ms   396 us     0 ns
Avg: 771 ms   632 us   400 ns
Max: 785 ms   270 us     0 ns
Std:   4 ms   825 us   277 ns

Set number of threads to 64
Timer results with precision: 1000000

Pagerank timer (ran 10x) (10): 
Min: 419 ms   400 us     0 ns
Avg: 423 ms   228 us   100 ns
Max: 451 ms   987 us     0 ns
Std:  10 ms   108 us   709 ns

Set number of threads to 128
Timer results with precision: 1000000

Pagerank timer (ran 10x) (10): 
Min: 224 ms    75 us     0 ns
Avg: 229 ms   602 us   200 ns
Max: 275 ms   545 us     0 ns
Std:  16 ms   144 us   768 ns

Set number of threads to 256
Timer results with precision: 1000000

Pagerank timer (ran 10x) (10): 
Min: 170 ms   683 us     0 ns
Avg: 188 ms   896 us   800 ns
Max: 318 ms   256 us     0 ns
Std:  45 ms   883 us   747 ns

